import Combine

@resultBuilder
public enum Builder {
	public static func buildBlock(
		_ cancellables: AnyCancellable...
	) -> [AnyCancellable] {
		cancellables
	}
}
