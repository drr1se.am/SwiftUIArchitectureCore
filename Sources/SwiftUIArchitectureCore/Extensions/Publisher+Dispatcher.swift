import Combine

extension Publisher where Self.Failure == Never {
	public func asyncSink(_ receiveValue: @escaping (Self.Output) async -> Void) -> AnyCancellable {
		sink { output in
			Task {
				await receiveValue(output)
			}
		}
	}
	
	public func dispatchOn<State: Dispatcher>(
		_ dispatcher: State
	) -> AnyCancellable {
		compactMap { $0 as? State.Action }
			.asyncSink { [dispatcher] action in
				await dispatcher.dispatch(action)
			}
	}
}
