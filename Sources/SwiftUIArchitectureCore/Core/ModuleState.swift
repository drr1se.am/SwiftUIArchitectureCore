import Combine

public protocol ModuleState: ObservableObject, Dispatcher {
	associatedtype Option
	
	var publisher: AnyPublisher<Option, Never> { get }
}

public extension ModuleState {
	func onDeinit(action: @escaping () -> Void) {
		DeathRattle<Self>.onDeathOf(self, action: action)
	}
}
