import Combine

public class StateBinding<SomeState> where SomeState: ModuleState {
	private let lifeSpanObject: AnyObject
	private let publisher: AnyPublisher<SomeState.Option, Never>
	private let dispatcher: AnyDispatcher<SomeState>
	
	public init(
		lifeSpanObject: AnyObject,
		publisher: AnyPublisher<SomeState.Option, Never>,
		dispatcher: AnyDispatcher<SomeState>
	) {
		self.lifeSpanObject = lifeSpanObject
		self.publisher = publisher
		self.dispatcher = dispatcher
	}
	
	public func retainSubscriptions(
		@Builder _ cancellables: (
			AnyPublisher<SomeState.Option, Never>,
			AnyDispatcher<SomeState>
		) -> [AnyCancellable]
	) {
		let subscriptions = cancellables(publisher, dispatcher)
		DeathRattle<Self>.retainTillDeathOf(lifeSpanObject, subject: subscriptions)
	}
}

public extension ModuleState {
	func eraseToStateBinding(lifeSpanObject: AnyObject) -> StateBinding<Self> {
		StateBinding(
			lifeSpanObject: lifeSpanObject,
			publisher: publisher,
			dispatcher: eraseToAnyDispatcher()
		)
	}
}
