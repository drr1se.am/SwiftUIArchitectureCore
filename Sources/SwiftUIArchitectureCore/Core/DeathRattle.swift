import Foundation
import ObjectiveC

public class DeathRattle<T> {
	private let action: () -> Void
	
	public init(action: @escaping () -> Void) {
		self.action = action
	}
	
	public static func onDeathOf<Object>(
		_ object: Object,
		action: @escaping () -> Void
	) {
		objc_setAssociatedObject(
			object,
			"\(Object.self)-action",
			DeathRattle<Object>(action: action),
			.OBJC_ASSOCIATION_RETAIN_NONATOMIC
		)
	}
	
	public static func retainTillDeathOf<Object: AnyObject, Subject>(
		_ object: Object,
		subject: Subject
	) {
		objc_setAssociatedObject(
			object,
			"\(Object.self)-object",
			subject,
			.OBJC_ASSOCIATION_RETAIN_NONATOMIC
		)
	}
	
	deinit {
		action()
	}
}
