import Foundation

public protocol Dispatcher {
	associatedtype Action
	
	@MainActor func dispatch(_ action: Action) async
}

public class AnyDispatcher<SomeDispatcher>: Dispatcher where SomeDispatcher: Dispatcher {
	private let dispatcher: SomeDispatcher
	
	public init(dispatcher: SomeDispatcher) {
		self.dispatcher = dispatcher
	}
	
	@MainActor public func dispatch(_ action: SomeDispatcher.Action) async {
		await dispatcher.dispatch(action)
	}
}

public extension Dispatcher {
	func eraseToAnyDispatcher() -> AnyDispatcher<Self> {
		AnyDispatcher(dispatcher: self)
	}
}
